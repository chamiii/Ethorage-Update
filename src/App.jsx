import { Navi, Home, Footer, Services, DecentStore} from "./pages" ;

const App = () => {
  return (
    <div className="min-h-screen gradient-bg-welcome" >
      <div>
        <Navi/>
        <Home/>
      </div>
      <div>
      <DecentStore/>
      </div>
      <Services/>
      
      
      <Footer/>
    </div>
  );
}

export default App;
