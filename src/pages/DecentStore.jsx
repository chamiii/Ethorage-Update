import { useState } from 'react'
import { create as ipfsHttpClient } from "ipfs-http-client";
import Loader from "./Loader";

const projectId = "2IcUYVTeR3CszfUkb6LBjtVY5bv"
const projectSecretKey = "1d348c1d9fecb2a2b1c4792860734151"
const authorization = "Basic " + btoa(projectId + ":" + projectSecretKey);

const DecentStore = () => {
    const [images, setImages] = useState([])
  const ipfs = ipfsHttpClient({
    url: "https://ipfs.infura.io:5001/api/v0",
    headers: {
      authorization
    }
  })

  const handleSubmit = async (event) => {
    event.preventDefault();
    const form = event.target;
    const files = (form[0]).files;

    if (!files || files.length === 0) {
      return alert("No files selected");
    }

    
    const file = files[0];
    // upload files
    const result = await ipfs.add(file);

    setImages([
      ...images,
      {
        cid: result.cid,
        path: result.path,
      },
    ]);
    form.reset();
  };


    return (
        <div className="w-full justify-center items-center">
        <div className="flex mf:flex-row flex-col items-center justify-between md:p-20 py-12 px-4">
        <div className="upload p-1 items-center blue-glassmorphism">
            <div className="p-5 sm:w-96 flex flex-col justify-start items-center gradient-bg-services">
                <div className="App">
                    {ipfs && (
                        <>
                        <form onSubmit={handleSubmit}>
                        
                            <label for = "file1" >
                            <div className="flex justify-center py-1 my-3 bg-[#2546bd] p-1 rounded-full cursor-pointer white"><p style={{ color: "white" }}> Upload Your Files Here ! </p></div>
                            </label>
                            <input id="file1" type="file" name="file" hidden />
                            <button type="submit"><Loader/></button>
                    
                        </form>
                        </>
                        )}
                </div>
            </div>
        </div>
        <div className="flex flex-col flex-1 items-center justify-end w-full mf:mt-0 mt-10">
        <div className="justify-center items-center light rounded-lg" >
            <div className="p-3 sm:w-96 flex flex-col justify-center items-center"><p style={{color:"white"}}>Your Uploaded Files Are Displayed Here</p></div>
            {images.map((image, index) => (

                <img
                alt={`Uploaded #${index + 1}`}
                src={"https://skywalker.infura-ipfs.io/ipfs/" + image.path}
                style={{ maxWidth: "330px", margin: "15px" }}
                key={image.cid.toString() + index}
                />
            ))}
        </div>
    </div>
    </div>
    </div>
    );
}

export default DecentStore;