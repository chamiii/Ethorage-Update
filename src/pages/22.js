const sun = document.querySelector(".sun");
const moon = document.querySelector(".moon");

const userTheme = localStorage.getItem("theme");
const sysTheme = window.matchMedia("(prefers-color-scheme: dark)").matches;

const iconToggle = () => {
    moon.classList.toggle("display-non");
    sun.classList.toggle("display-non");
};

const themeCheck = () => {
    if (userTheme === "dark" || (!userTheme && sysTheme)){
        document.documentElement.classList.add("dark");
        moon.classList.add("display-non");
        return;
    }
    sun.classList.add("display-non");
};

const themeSwitch = () => {
    if (document.documentElement.classList.contains("dark")){
        document.documentElement.classList.remove("dark");
        localStorage.setItem("theme", "light");
        iconToggle();
        return;
    }
    document.documentElement.classList.add("dark");
    localStorage.setItem("theme", "dark");
    iconToggle();
};

sun.addEventListener("click", () => {
    themeSwitch();
});

moon.addEventListener("click", () => {
    themeSwitch();

});

themeCheck();
