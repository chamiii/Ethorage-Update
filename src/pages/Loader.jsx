import React from "react";
import {useState} from 'react';

import styles from "../styles";
import { ethereumLogo } from "../assets";

const Loader = () => {
  const [toggle,setToggle] = useState(true)
  return (
    <div>
      <button onClick={() => setToggle(toggle)}>
      {toggle && (
      <><img
          src={ethereumLogo}
          alt="ethereum logo"
          className={styles.loaderImg} /><p style={{ color: "white" }}> Click Eth to Store Files </p></>
      )}
    </button>  
    </div>
  );
};

export default Loader;