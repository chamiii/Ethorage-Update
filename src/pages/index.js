export { default as Loader } from './Loader';
export { default as Navi } from './Navi';
export { default as Footer } from './Footer';
export { default as Home } from './Home';
export { default as Services } from './Services';
export { default as DecentStore } from './DecentStore';